# Graphbuilder  

---

Set of resources to produce diagrams and synoptics with FME from a graph structure: set of nodes and arcs.  
These tools do not exploit the geometry of the data but their topological or attribute relationships.  
Typical output formats are pdf, excel or autocad.  

---  

